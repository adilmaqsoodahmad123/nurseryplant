@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{asset('assets/frontend/img/banner/banner_bg.jpg')}}');">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Return Policy</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">Return Policy</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== SHOP SECTION START ======-->
    <section class="Shop-section pt-120 pb-120">
        <div class="container">
            <div class="row ">
                <!-- Shop Sidebar -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="shop-products-wrapper">

                        <div class="product-wrapper restaurant-tab-area">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12 "></div>
                                <p>
                                    {{$site_settings != null && $site_settings->return_policy != null ? $site_settings->return_policy : 'No Record Found!'}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== SHOP SECTION END ======-->
@endsection
