@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{asset('assets/frontend/img/banner/banner_bg.jpg')}}');">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Sign Up</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Sign Up</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== SHOP SECTION START ======-->
    <section class="login-sec pt-120 pb-120">
        <div class="container">
            <div class="account-wrapper">
                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="login-content" style="background-image: url('{{asset('assets/frontend/img/Jade_Plant.jpg')}}');">
                            <div class="description text-center">
                                <h2>Welcome!</h2>
                                <p class="text-white">Create Your New Account & Buy Your Desired</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="login-form">
                            <h2 class="text-dark">Sign Up</h2>
                            @if(session()->has('error-msg'))
                            <div class="alert alert-danger">
                                {{session('error-msg')}}
                            </div>
                            @endif
                            <form method="post" action="{{route('saveUser')}}">
                                @csrf
                                <div class="input-group input-group-two mb-20">
                                    <input type="text" placeholder="Full Name" name="name">
                                    @if($errors->has('name'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                                <div class="input-group input-group-two mb-20">
                                    <input type="email" placeholder="Email Address" name="email">
                                    @if($errors->has('email'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                                <div class="input-group input-group-two mb-20">
                                    <input type="text" placeholder="Phone No" name="phone">
                                    @if($errors->has('phone'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('phone')}}</span>
                                    @endif
                                </div>
                                <div class="input-group input-group-two mb-30">
                                    <input type="password" placeholder="Password" name="password">
                                    @if($errors->has('password'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <button type="submit" class="main-btn btn-filled mt-20 login-btn">Signup</button>
                                <div class="form-seperator">
                                    <span>OR</span>
                                </div>
{{--                                <div class="social-buttons">--}}
{{--                                    <button type="button" class="main-btn btn-border facebook mb-20">--}}
{{--                                        <i class="fab fa-facebook-f"></i>--}}
{{--                                        Continue with Facebook--}}
{{--                                    </button>--}}
{{--                                    <button type="button" class="main-btn btn-filled mb-30">--}}
{{--                                        <i class="fab fa-google"></i>--}}
{{--                                        Continue with Google--}}
{{--                                    </button>--}}
{{--                                </div>--}}
                                <p>Already have an Account?
                                    <a href="{{route('signIn')}}" class="d-inline-block text-dark">Login</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== SHOP SECTION END ======-->
@endsection
