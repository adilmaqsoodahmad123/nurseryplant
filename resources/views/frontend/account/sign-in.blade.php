@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{asset('assets/frontend/img/banner/banner_bg.jpg')}}');">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Sign In</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Sign In</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== SHOP SECTION START ======-->
    <section class="login-sec pt-120 pb-120">
        <div class="container">
            <div class="account-wrapper">
                <div class="row no-gutters">
                    <div class="col-lg-6">
                        <div class="login-content" style="background-image: url('{{asset('assets/frontend/img/Jade_Plant.jpg')}}');">
                            <div class="description text-center">
                                <h2>Welcome!</h2>
                                <p class="text-white">Sign In to Your Account!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="login-form">
                            <h2 class="text-dark">Sign In</h2>
                            @if(session()->has('error-msg'))
                                <div class="alert alert-danger">
                                    {{session('error-msg')}}
                                </div>
                            @endif
                            <form method="post" action="{{route('userLogin')}}">
                                @csrf
                                <div class="input-group input-group-two mb-20">
                                    <input type="email" placeholder="Email Address" name="email">
                                    @if($errors->has('email'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                                <div class="input-group input-group-two mb-30">
                                    <input type="password" placeholder="Password" name="password">
                                    @if($errors->has('password'))
                                        <span style="color: red; font-size: 12px">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                                <a href="#" class="text-dark">Forgot Password?</a>
                                <button type="submit" class="main-btn btn-filled mt-20 login-btn">SignIn</button>
                                <div class="form-seperator">
                                    <span>OR</span>
                                </div>

                                <p>Don't have an Account?
                                    <a href="{{route('signUp')}}" class="d-inline-block text-dark">Create One</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== SHOP SECTION END ======-->
@endsection
