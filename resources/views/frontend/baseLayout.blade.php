<!DOCTYPE html>
<html>
<head>
  <!--====== Required meta tags ======-->
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="csrf-token" content="{{csrf_token()}}">
  <!--====== Title ======-->
  <title>@yield('title')</title>
  <!--====== Animate Css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/animate.min.css')}}">
  <!--====== Bootstrap css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.min.css')}}" />
  <!--====== Fontawesome css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/font-awesome.min.css')}}" />
  <!--====== Flaticon css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/flaticon.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/frontend/fonts/flaticon/flaticon-2.css')}}" />

    <link rel="stylesheet" href="{{asset('assets/frontend/fonts/futura/Futura Light font.ttf')}}" />
  <!--====== Magnific Popup css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/magnific-popup.css')}}" />
  <!--====== Owl Carousel css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/slick.css')}}" />
  <!--====== Nice Select ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/nice-select.css')}}" />
  <!--====== Mapbox ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/leaflet.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/frontend/css/mapbox-gl.min.css')}}" />
  <!--====== Bootstrap Datepicker ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap-datepicker.css')}}" />
  <!--====== Ion Rangeslider ======-->
    <link rel="icon" href="{{asset('assets/frontend/img/fav.png')}}">
  <link rel="stylesheet" href="{{asset('assets/frontend/css/ion.rangeSlider.min.css')}}" />
  <!--====== Default css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/default.css')}}" />
  <!--====== Style css ======-->
  <link rel="stylesheet" href="{{asset('assets/frontend/css/style.css')}}" />
    @yield('styles')
</head>

<body>




<!--====== HEADER START ======-->
<header class="header-absolute header-two plus-three sticky-header sigma-header">
  <div class="lr-topbar">
    <div class="container container-custom-one">
      <div class="row align-items-center">
        <div class="col-lg-4">
          <div class="lr-topbar-controls">
            <ul>
              <li>

              </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="lr-topbar-controls style-2">
            <ul>
              <li>
                <a href="{{route('myAccount')}}">
                  My Account
                </a>
              </li>
                @if(!auth()->check())
              <li>
                <a href="{{route('signUp')}}">
                  Signup
                </a>
              </li>
              <li>
                <a href="{{route('signIn')}}">
                  Login
                </a>
              </li>
                @else
                    <li>
                        <a href="{{route('userLogout')}}">
                            Logout
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{route('cart')}}">
                        <i class="fa fa-shopping-cart"></i> Cart
                    </a>
                </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container container-custom-one">
    <div class="nav-container d-flex align-items-center">
      <!-- Main Menu -->
      <div class="nav-menu d-lg-flex align-items-center justify-content-center">
        <!-- Navbar Close Icon -->
        <div class="navbar-close">
          <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
        </div>

        <!-- Mneu Items -->
        <div class="sigma-header-nav">
          <div class="sigma-header-nav-inner">
            <nav>
              <ul class="sigma-main-menu">
                <li class="menu-item ">
                  <a href="{{url('/')}}">
                    Home
                  </a>
                </li>
                <li class="menu-item menu-item-has-children">
                  <a href="#">
                    Shop
                  </a>
                  <ul class="sub-menu">
                    <li class="menu-item">
                      <a href="{{route('allProducts')}}">New Arrival</a>
                    </li>
                      @if(count($categories) > 0)
                          @foreach($categories as $category)
                             <li class="menu-item">
                              <a href="{{route('allProductByCategory',$category->id)}}">{{$category->name}}</a>
                            </li>
                          @endforeach
                      @endif
                  </ul>
                </li>
                <li class="site-logo site-logo-text">
                  <a href="{{url('/')}}">
{{--                    <img src="{{asset('assets/frontend/img/zlogo.png')}}" width="60" height="60">--}}
{{--                    <div class="site-logo-text" style="margin-left: 3px !important;">--}}
{{--                      <h3 style="color: #FFFD98 !important;">aida's</h3>--}}
{{--                      <h6 class="font-italic" style="color: #FFFD98 !important;">KloZet</h6>--}}
{{--                    </div>--}}
                  </a>
                </li>
                <li class="menu-item menu-item-has-children">
                  <a href="#">
                    Menu
                  </a>
                  <ul class="sub-menu">
                    <li class="menu-item">
                      <a href="{{route('shippingPolicy')}}">Shipping</a>
                    </li>
                    <li class="menu-item">
                      <a href="{{route('returnPolicy')}}">Return Policy</a>
                    </li>
                  </ul>
                </li>
                <li class="menu-item">
                  <a href="#">
                    Contact
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- from pushed-item -->
        <div class="nav-pushed-item"></div>
      </div>
      <!-- Site Logo -->
{{--      <div class="site-logo site-logo-text d-block d-lg-none">--}}
{{--        <a href="{{url('/')}}">--}}
{{--          <img src="{{asset('assets/frontend/img/zlogo.png')}}" width="60" height="60">--}}
{{--          <div class="site-logo-text" style="margin-left: 3px !important;">--}}
{{--            <h3>aida's</h3>--}}
{{--            <h6 class="font-italic">KloZet</h6>--}}
{{--          </div>--}}
{{--        </a>--}}
{{--      </div>--}}
      <!-- Navbar Toggler -->
      <div class="sigma-mobile-header">
        <div class="container">
          <div class="sigma-mobile-header-inner">
            <div class="sigma-hamburger-menu" style="color: #8D6724">
              <div class="sigma-menu-btn" style="background-color: #8D6724 ">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Mobile Menu Start -->
      <aside class="sigma-mobile-menu">
        <ul class="sigma-main-menu">
          <li class="menu-item">
            <a href="{{url('/')}}">
              Home
            </a>
          </li>
          <li class="menu-item menu-item-has-children">
            <a href="#">
              Shop
            </a>
            <ul class="sub-menu">
              <li class="menu-item">
                <a href="{{route('allProducts')}}">New Arrival</a>
              </li>
                @if(count($categories) > 0)
                    @foreach($categories as $category)
                        <li class="menu-item">
                <a href="{{route('allProductByCategory',$category->id)}}">{{$category->name}}</a>
              </li>
                    @endforeach
                @endif
            </ul>
          </li>
          <li class="menu-item">
            <a href="#">
              Contact
            </a>
          </li>
          <li class="menu-item">
            <a href="{{route('shippingPolicy')}}">
              Shipping
            </a>
          </li>
          <li class="menu-item">
            <a href="{{route('returnPolicy')}}">
              Return Policy
            </a>
          </li>
            @if(!auth()->check())
            <li class="menu-item">
                <a href="{{route('signUp')}}">
                    Sign Up
                </a>
            </li>
            <li class="menu-item">
                <a href="{{route('signIn')}}">
                    Login
                </a>
            </li>
            @else
                <li class="menu-item">
                    <a href="{{route('myAccount')}}">
                        My Account
                    </a>
                </li>
                <li class="menu-item">
                    <a href="{{route('userLogout')}}">
                        Logout
                    </a>
                </li>
            @endif
            <li class="menu-item">
                <a href="{{route('cart')}}">
                    Cart
                </a>
            </li>
        </ul>
      </aside>
      <!-- Mobile Menu End -->
    </div>
  </div>
</header>
<!--====== HEADER END ======-->
@yield('main-content')

  <!--====== Back to Top ======-->
  <a href="#" class="back-to-top" id="backToTop">
    <i class="fal fa-angle-double-up"></i>
  </a>
  <!--====== FOOTER START ======-->
  <footer class="footer-two" style="border-top: 1px solid lightgrey">
    <div class="footer-widget-area pt-100 ">
      <div class="container">
        <div class="row justify-content-around">
          <div class="col-lg-7 col-sm-7">
            <!-- Contact Widget -->
            <div class="widget contact-widget mb-50">
              <h4 class="widget-title">Contact Us.</h4>
                @if(session()->has('success-mail'))
                <span>{{session('success-mail')}}</span>
                @endif
                <form action="{{route('contactInquiry')}}" method="post">
                    @csrf
                <div class="row">
                    <div class="col-xl-4 input-group input-group-two mb-20">
                        <label style="color: #000000 !important;">Name</label>
                        <input type="text" placeholder="Enter Name" name="name" required="">
                    </div>
                    <div class="col-xl-4 input-group input-group-two mb-20">
                        <label style="color: #000000 !important;">Email</label>
                        <input type="email" placeholder="Enter Email" name="email" required="">
                    </div>
                    <div class="col-xl-4 input-group input-group-two mb-20">
                        <label style="color: #000000 !important;">Phone</label>
                        <input type="text" placeholder="Enter Phone" name="phone" required="">
                    </div>
                    <div class="col-12 input-group input-group-two mb-20">
                        <label style="color: #000000 !important;">Message</label>
                        <textarea style="min-height: 100px !important;" type="email" placeholder="Enter Message" name="message" required=""></textarea>
                    </div>
                    <div class="col-12">
                        <button type="submit" class="main-btn btn-filled w-100">Submit</button>
                    </div>
                </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </footer>
  <!--====== FOOTER END ======-->

  <!--====== Modal Popup Start ======-->
  <!-- The Modal -->
{{--  <div class="modal fade on-load-modal" id="myModal">--}}
{{--    <div class="modal-dialog modal-dialog-centered">--}}
{{--      <div class="modal-content" style="background-image: url(assets/img/popup.jpg)">--}}
{{--        <!-- Modal Header -->--}}
{{--        <div class="modal-header">--}}
{{--          <button type="button" class="close popup-trigger" data-dismiss="modal">&times;</button>--}}
{{--        </div>--}}
{{--        <!-- Modal body -->--}}
{{--        <div class="modal-body">--}}
{{--          <div class="modal-inner">--}}
{{--            <h3 class="title">Newsletter</h3>--}}
{{--            <p>Subscribe to our newsletter to recieve exclusive offers</p>--}}

{{--            <form>--}}
{{--              <input type="email" placeholder="Email Address" name="email" value="">--}}
{{--              <button type="submit" class="main-btn btn-filled" name="button">Subscribe</button>--}}
{{--            </form>--}}

{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--    </div>--}}
{{--  </div>--}}
  <!--====== Modal Popup End ======-->

  <!--====== jquery js ======-->
  <script src="{{asset('assets/frontend/js/vendor/modernizr-3.6.0.min.js')}}"></script>
  <script src="{{asset('assets/frontend/js/vendor/jquery-1.12.4.min.js')}}"></script>
  <!--====== Bootstrap js ======-->
  <script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/frontend/js/popper.min.js')}}"></script>
  <!--====== Slick js ======-->
  <script src="{{asset('assets/frontend/js/slick.min.js')}}"></script>
  <!--====== Isotope js ======-->
  <script src="{{asset('assets/frontend/js/isotope.pkgd.min.js')}}"></script>
  <!--====== Magnific Popup js ======-->
  <script src="{{asset('assets/frontend/js/jquery.magnific-popup.min.js')}}"></script>
  <!--====== inview js ======-->
  <script src="{{asset('assets/frontend/js/jquery.inview.min.js')}}"></script>
  <!--====== counterup js ======-->
  <script src="{{asset('assets/frontend/js/jquery.countTo.js')}}"></script>
  <!--====== Nice Select ======-->
  <script src="{{asset('assets/frontend/js/jquery.nice-select.min.js')}}"></script>
  <!--====== Bootstrap datepicker ======-->
  <script src="{{asset('assets/frontend/js/bootstrap-datepicker.js')}}"></script>
  <!--====== Ion Rangeslider ======-->
  <script src="{{asset('assets/frontend/js/ion.rangeSlider.min.js')}}"></script>
  <!--====== Jquery Countdown ======-->
  <script src="{{asset('assets/frontend/js/jquery.countdown.min.js')}}"></script>
  <!--====== Wow JS ======-->
  <script src="{{asset('assets/frontend/js/wow.min.js')}}"></script>
  <!--====== Mapbox Map ======-->
  <script src="{{asset('assets/frontend/js/leaflet.js')}}"></script>
  <script src="{{asset('assets/frontend/js/mapbox-gl.min.js')}}"></script>
  <script src="{{asset('assets/frontend/js/map.js')}}"></script>
  <!--====== Main js ======-->
  <script src="{{asset('assets/frontend/js/main.js')}}"></script>
@yield('scripts')
</body>
</html>
