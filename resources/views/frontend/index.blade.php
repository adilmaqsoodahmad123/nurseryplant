@extends('frontend.baseLayout')
@section('title',"Nursery Plants")
@section('main-content')
    <!--====== BANNER PART START ======-->
    <section class="banner-area banner-style-two" id="bannerSlider">

        <div class="single-banner d-flex align-items-center justify-content-center">

            <!-- Follow Circle -->
            <div class="circle-out"></div>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="banner-content text-center">
                            <span class="promo-tag" data-animation="fadeInDown" data-delay=".6s">
                                {{$site_settings != null && $site_settings->home_text_tagline != null ? $site_settings->home_text_tagline : 'Yes! Our Plants are setting trends'}}
                            </span>
                            <h5 class="title" data-animation="fadeInLeft" data-delay=".9s">
                                {{$site_settings != null && $site_settings->home_text_heading != null ? $site_settings->home_text_heading : 'Gorgeous Indoor Plants That Are Almost Impossible to Kill.'}}
                            </h5>
                            <ul>
                                <li data-animation="fadeInUp" data-delay="1.1s">
                                    <a class="main-btn btn-filled" href="{{route('allProducts')}}">explore more</a>
                                </li>
                                <li data-animation="fadeInUp" data-delay="1.3s">
                                    <a class="main-btn btn-border" href="{{route('allProducts')}}">Buy Now</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- banner bg -->
            <div class="banner-bg" style="background-image: url('{{$site_settings != null && $site_settings->home_image != null ? asset('assets/frontend/img/banner/'.$site_settings->home_image) : asset('assets/frontend/img/banner/banner_bg.jpg')}}'); background-position: inherit; background-size: cover;background-repeat: no-repeat "></div>
            <div class="banner-overly"></div>
        </div>
        <!--====== BANNER PART ENDS ======-->
    <!--====== Category START ======-->
    <section  class="restaurant-tab-area  pt-115 pb-85">
        <div class="container">
            <div class="category-box-sec pb-155 ">
                <h2 class="text-center py-3">New Arrivals</h2>
                <div class="row no-gutters px-5" id="n-arrival">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            <div class=" col-lg-4 col-md-4 col-sm-6 col-6 ">
                        <a href="{{route('singleProduct',$product->slug)}}"> <img src="{{asset('assets/frontend/img/product/'.$product->medias[0]->image)}}" alt="img" class="img-fluid"></a>
                        <p class="text-center new-arrival-text-style">
                            <a href="{{route('singleProduct')}}" style="color: #111111 !important;">
                                {{$product->name}}
                            </a>
                        </p>
                        <p class="text-center new-arrival-text-style" style="color: #000000 !important;">${{$product->price}}</p>
                    </div>
                        @endforeach
                    @endif

                    <div class="col-md-12 text-center pt-2">
                        <a href="{{route('allProducts')}}" class="btn btn-sm new-arrival-text-style" style="background-color: #111111;color: #ffffff !important;">View All</a>
                    </div>
                    <!--        <div class="col-lg-6">-->
                    <!--          <div class="category-box-wrap">-->
                    <!--            <div class="category-box-image">-->
                    <!--              <img src="assets/img/img2.jpeg" alt="img">-->
                    <!--            </div>-->
                    <!--            <div class="category-box-content">-->
                    <!--              <div class="content text-center">-->
                    <!--                <h3 class="title"><a href="#">Clothes</a></h3>-->
                    <!--                <a href="#" class="main-btn btn-filled">View Details</a>-->
                    <!--              </div>-->
                    <!--            </div>-->
                    <!--          </div>-->
                    <!--        </div>-->
                    <!--        <div class="col-lg-6">-->
                    <!--          <div class="category-box-wrap">-->
                    <!--            <div class="category-box-image">-->
                    <!--              <img src="assets/img/img18.jpeg" alt="img">-->
                    <!--            </div>-->
                    <!--            <div class="category-box-content">-->
                    <!--              <div class="content text-center">-->
                    <!--                <h3 class="title"><a href="#">Best Summer Sale</a></h3>-->
                    <!--                <a href="#" class="main-btn btn-filled">View Details</a>-->
                    <!--              </div>-->
                    <!--            </div>-->
                    <!--          </div>-->
                    <!--        </div>-->
                    <!--        <div class="col-lg-6">-->
                    <!--          <div class="category-box-wrap">-->
                    <!--            <div class="category-box-content">-->
                    <!--              <div class="content text-center">-->
                    <!--                <h3 class="title"><a href="#">Handbags</a></h3>-->
                    <!--                <a href="#" class="main-btn btn-filled">View Details</a>-->
                    <!--              </div>-->
                    <!--            </div>-->
                    <!--            <div class="category-box-image">-->
                    <!--              <img src="assets/img/img15.jpeg" alt="img">-->
                    <!--            </div>-->
                    <!--          </div>-->
                    <!--        </div>-->
                    <!--        <div class="col-lg-6">-->
                    <!--          <div class="category-box-wrap">-->
                    <!--            <div class="category-box-content">-->
                    <!--              <div class="content text-center">-->
                    <!--                <h3 class="title"><a href="#">Dress</a></h3>-->
                    <!--                <a href="#" class="main-btn btn-filled">View Details</a>-->
                    <!--              </div>-->
                    <!--            </div>-->
                    <!--            <div class="category-box-image">-->
                    <!--              <img src="assets/img/img9.jpeg" alt="img">-->
                    <!--            </div>-->
                    <!--          </div>-->
                    <!--        </div>-->
                </div>
            </div>
        </div>
    </section>
    <!--====== Category EN ======-->

{{--    <section class="condos-overlay-sec pt-115 pb-155">--}}
{{--        <div class="container">--}}
{{--            <div class="section-title text-center mb-50">--}}
{{--                <h3 class="text-center py-3" style="color: #000000 !important;">Shop By Category</h3>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-3 col-md-6">--}}
{{--                        <div class="condo-item hotel-intro" style="background-image: url('{{asset('assets/frontend/img/Jade_Plant.jpg')}}');">--}}
{{--                            <a href="{{route('clothingCollection')}}">--}}
{{--                                <div class="title">--}}
{{--                                    <h4 class="text-white">Clothes</h4>--}}
{{--                                    <div class="display-on-hover">--}}
{{--                                        <p class="text-white">Get your wedding clothes</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                <div class="col-lg-3 col-md-6">--}}
{{--                    <div class="condo-item hotel-intro" style="background-image: url('{{asset('assets/frontend/img/Jade_Plant.jpg')}}');">--}}
{{--                        <a href="{{route('handbagsCollection')}}">--}}
{{--                            <div class="title">--}}
{{--                                <h4 class="text-white">Hangbags</h4>--}}
{{--                                <div class="display-on-hover">--}}
{{--                                    <p class="text-white">We have modern design handbags , get now </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6">--}}
{{--                    <div class="condo-item hotel-intro" style="background-image: url({{asset('assets/frontend/img/shoes.jpg')}});">--}}
{{--                        <a href="{{route('shoesCollection')}}">--}}
{{--                            <div class="title">--}}
{{--                                <h4 class="text-white">Shoes</h4>--}}
{{--                                <div class="display-on-hover">--}}
{{--                                    <p class="text-white">It is new trend in in shoes  </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6">--}}
{{--                    <div class="condo-item hotel-intro" style="background-image: url('{{asset('assets/frontend/img/img15.jpeg')}}');">--}}
{{--                        <a href="{{route('accessoriesCollection')}}">--}}
{{--                            <div class="title">--}}
{{--                                <h4 class="text-white">Accessories</h4>--}}
{{--                                <div class="display-on-hover">--}}
{{--                                    <p class="text-white">New available accessories</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!--====== ROOM TYPE START ======-->
{{--    <section class="room-type-section pt-115 pb-115" style="background-image: url('{{asset('assets/frontend/img/bg/01.jpg')}}');">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="section-title text-lg-left text-center">--}}
{{--                        <h2>Sales you can't resist </h2>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <ul class="room-filter nav nav-pills justify-content-center justify-content-lg-end" id="room-tab" role="tablist">--}}

{{--                         <li class="nav-item">--}}
{{--                                <a class="nav-link active" id="clothing-tab" data-toggle="pill" href="#clothing">Clothing</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" id="handbags-tab" data-toggle="pill" href="#handbags">Handbags</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" id="shoes-tab" data-toggle="pill" href="#shoes">Shoes</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" id="accessories-tab" data-toggle="pill" href="#accessories"> Accessories</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="tab-content mt-65" id="room-tabContent">--}}
{{--                <div class="tab-pane fade show active" id="clothing" role="tabpanel">--}}
{{--                    <div class="room-items">--}}
{{--                        <div class="row">--}}
{{--                            @if(count($clothing_data) > 0)--}}
{{--                            <div class="col-lg-8">--}}
{{--                                    <div class="row">--}}
{{--                                        @if(isset($clothing_data[0]))--}}
{{--                                        <div class="col-12">--}}
{{--                                            <div class="room-box extra-wide">--}}
{{--                                                <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$clothing_data[0]->medias[0]->image)}}');"></div>--}}
{{--                                                <div class="room-content">--}}
{{--                                                    <span class="room-count"><i class="fa fa-dollar">$</i>{{$clothing_data[0]->price}}</span>--}}
{{--                                                    <h3><a href="{{route('singleProduct',$clothing_data[0]->slug)}}">{{$clothing_data[0]->name}}</a></h3>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{route('singleProduct',$clothing_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($clothing_data[1]))--}}
{{--                                        <div class="col-lg-6 col-sm-6">--}}
{{--                                            <div class="room-box">--}}
{{--                                                <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$clothing_data[1]->medias[0]->image)}}');">--}}
{{--                                                </div>--}}
{{--                                                <div class="room-content">--}}
{{--                                                    <span class="room-count"><i class="fa fa-dollar">$</i>{{$clothing_data[1]->price}}</span>--}}
{{--                                                    <h3><a href="{{route('singleProduct',$clothing_data[0]->slug)}}">{{$clothing_data[1]->name}}</a></h3>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{route('singleProduct',$clothing_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($clothing_data[2]))--}}
{{--                                        <div class="col-lg-6 col-sm-6">--}}
{{--                                            <div class="room-box">--}}
{{--                                                <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$clothing_data[2]->medias[0]->image)}}');">--}}
{{--                                                </div>--}}
{{--                                                <div class="room-content">--}}
{{--                                                    <span class="room-count"><i class="fa fa-dollar">$</i>{{$clothing_data[2]->price}}</span>--}}
{{--                                                    <h3><a href="{{route('singleProduct',$clothing_data[0]->slug)}}">{{$clothing_data[2]->name}}</a></h3>--}}
{{--                                                </div>--}}
{{--                                                <a href="{{route('singleProduct',$clothing_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @if(isset($clothing_data[3]))--}}
{{--                            <div class="col-lg-4">--}}
{{--                                <div class="room-box extra-height">--}}
{{--                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$clothing_data[3]->medias[0]->image)}}');">--}}
{{--                                    </div>--}}
{{--                                    <div class="room-content">--}}
{{--                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$clothing_data[3]->price}}</span>--}}
{{--                                        <h3><a href="{{route('singleProduct',$clothing_data[0]->slug)}}">{{$clothing_data[3]->name}}</a></h3>--}}
{{--                                    </div>--}}
{{--                                    <a href="{{route('singleProduct',$clothing_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                                @endif--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="tab-pane fade " id="handbags" role="tabpanel">--}}
{{--                    <div class="room-items">--}}
{{--                        <div class="row">--}}
{{--                            @if(count($handbags_data) > 0)--}}
{{--                                <div class="col-lg-8">--}}
{{--                                    <div class="row">--}}
{{--                                        @if(isset($handbags_data[0]))--}}
{{--                                            <div class="col-12">--}}
{{--                                                <div class="room-box extra-wide">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$handbags_data[0]->medias[0]->image)}}');"></div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$handbags_data[0]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$handbags_data[0]->slug)}}">{{$handbags_data[0]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$handbags_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($handbags_data[1]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$handbags_data[1]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$handbags_data[1]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$handbags_data[0]->slug)}}">{{$handbags_data[1]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$handbags_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($handbags_data[2]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$handbags_data[2]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$handbags_data[2]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$handbags_data[0]->slug)}}">{{$handbags_data[2]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$handbags_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @if(isset($handbags_data[3]))--}}
{{--                                    <div class="col-lg-4">--}}
{{--                                        <div class="room-box extra-height">--}}
{{--                                            <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$handbags_data[3]->medias[0]->image)}}');">--}}
{{--                                            </div>--}}
{{--                                            <div class="room-content">--}}
{{--                                                <span class="room-count"><i class="fa fa-dollar">$</i>{{$handbags_data[3]->price}}</span>--}}
{{--                                                <h3><a href="{{route('singleProduct',$handbags_data[0]->slug)}}">{{$handbags_data[3]->name}}</a></h3>--}}
{{--                                            </div>--}}
{{--                                            <a href="{{route('singleProduct',$handbags_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="tab-pane fade " id="shoes" role="tabpanel">--}}
{{--                    <div class="room-items">--}}
{{--                        <div class="row">--}}
{{--                            @if(count($shoes_data) > 0)--}}
{{--                                <div class="col-lg-8">--}}
{{--                                    <div class="row">--}}
{{--                                        @if(isset($shoes_data[0]))--}}
{{--                                            <div class="col-12">--}}
{{--                                                <div class="room-box extra-wide">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$shoes_data[0]->medias[0]->image)}}');"></div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$shoes_data[0]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$shoes_data[0]->slug)}}">{{$shoes_data[0]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$shoes_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($shoes_data[1]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$shoes_data[1]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$shoes_data[1]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$shoes_data[0]->slug)}}">{{$shoes_data[1]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$shoes_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($shoes_data[2]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$shoes_data[2]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$shoes_data[2]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$shoes_data[0]->slug)}}">{{$shoes_data[2]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$shoes_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @if(isset($shoes_data[3]))--}}
{{--                                    <div class="col-lg-4">--}}
{{--                                        <div class="room-box extra-height">--}}
{{--                                            <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$shoes_data[3]->medias[0]->image)}}');">--}}
{{--                                            </div>--}}
{{--                                            <div class="room-content">--}}
{{--                                                <span class="room-count"><i class="fa fa-dollar">$</i>{{$shoes_data[3]->price}}</span>--}}
{{--                                                <h3><a href="{{route('singleProduct',$shoes_data[0]->slug)}}">{{$shoes_data[3]->name}}</a></h3>--}}
{{--                                            </div>--}}
{{--                                            <a href="{{route('singleProduct',$shoes_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="tab-pane fade " id="accessories" role="tabpanel">--}}
{{--                    <div class="room-items">--}}
{{--                        <div class="row">--}}
{{--                            @if(count($accessories_data) > 0)--}}
{{--                                <div class="col-lg-8">--}}
{{--                                    <div class="row">--}}
{{--                                        @if(isset($accessories_data[0]))--}}
{{--                                            <div class="col-12">--}}
{{--                                                <div class="room-box extra-wide">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$accessories_data[0]->medias[0]->image)}}');"></div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$accessories_data[0]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$accessories_data[0]->slug)}}">{{$accessories_data[0]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$accessories_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($accessories_data[1]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$accessories_data[1]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$accessories_data[1]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$accessories_data[0]->slug)}}">{{$accessories_data[1]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$accessories_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                        @if(isset($accessories_data[2]))--}}
{{--                                            <div class="col-lg-6 col-sm-6">--}}
{{--                                                <div class="room-box">--}}
{{--                                                    <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$accessories_data[2]->medias[0]->image)}}');">--}}
{{--                                                    </div>--}}
{{--                                                    <div class="room-content">--}}
{{--                                                        <span class="room-count"><i class="fa fa-dollar">$</i>{{$accessories_data[2]->price}}</span>--}}
{{--                                                        <h3><a href="{{route('singleProduct',$accessories_data[0]->slug)}}">{{$accessories_data[2]->name}}</a></h3>--}}
{{--                                                    </div>--}}
{{--                                                    <a href="{{route('singleProduct',$accessories_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                @if(isset($accessories_data[3]))--}}
{{--                                    <div class="col-lg-4">--}}
{{--                                        <div class="room-box extra-height">--}}
{{--                                            <div class="room-bg" style="background-image: url('{{asset('assets/frontend/img/product/'.$accessories_data[3]->medias[0]->image)}}');">--}}
{{--                                            </div>--}}
{{--                                            <div class="room-content">--}}
{{--                                                <span class="room-count"><i class="fa fa-dollar">$</i>{{$accessories_data[3]->price}}</span>--}}
{{--                                                <h3><a href="{{route('singleProduct',$accessories_data[0]->slug)}}">{{$accessories_data[3]->name}}</a></h3>--}}
{{--                                            </div>--}}
{{--                                            <a href="{{route('singleProduct',$accessories_data[0]->slug)}}" class="room-link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!--====== ROOM TYPE END ======-->

    <!--====== ROOM SLIDER START ======-->
{{--    <section class="room-slider">--}}
{{--        <div class="container-fluid p-0">--}}
{{--            <div class="row rooms-slider-one">--}}
{{--                <div class="col">--}}
{{--                    <div class="slider-img" style="background-image: url('{{asset('assets/frontend/assets/img/img11.jpeg')}}');">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col">--}}
{{--                    <div class="slider-img" style="background-image: url('{{asset('assets/frontend/img/img5.jpeg')}}');">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col">--}}
{{--                    <div class="slider-img" style="background-image: url('{{asset('assets/frontend/img/img18.jpeg')}}');">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col">--}}
{{--                    <div class="slider-img" style="background-image: url('{{asset('assets/frontend/img/img17.jpeg')}}');">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col">--}}
{{--                    <div class="slider-img" style="background-image: url('{{asset('assets/frontend//img/img3.jpeg')}}');">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="rooms-content-wrap">--}}
{{--            <div class="container">--}}
{{--                <div class="row justify-content-center justify-content-md-start">--}}
{{--                    <div class="col-xl-4 col-lg-5 col-sm-8">--}}
{{--                        <div class="room-content-box">--}}
{{--                            <div class="slider-count"></div>--}}
{{--                            <div class="slider-count-big"></div>--}}
{{--                            <div class="room-content-slider">--}}
{{--                                <div class="single-content">--}}
{{--                                    <h3><a href="#">Handbag</a></h3>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                        incididunt ut labore et dolore magna.</p>--}}
{{--                                </div>--}}
{{--                                <div class="single-content">--}}
{{--                                    <h3><a href="#">Handbag</a></h3>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                        incididunt ut labore et dolore magna.</p>--}}
{{--                                </div>--}}
{{--                                <div class="single-content">--}}
{{--                                    <h3><a href="#">Dress</a></h3>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                        incididunt ut labore et dolore magna.</p>--}}
{{--                                </div>--}}
{{--                                <div class="single-content">--}}
{{--                                    <h3><a href="#">Dress</a></h3>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                        incididunt ut labore et dolore magna.</p>--}}
{{--                                </div>--}}
{{--                                <div class="single-content">--}}
{{--                                    <h3><a href="#">Dress</a></h3>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                        incididunt ut labore et dolore magna.</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!--====== ROOM SLIDER END ======-->

    <!--======  END Hotel Intro ======-->
    <!--====== SHOP SECTION START ======-->
{{--    <section class="restaurant-tab-area  pt-115 pb-85">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6 col-md-8 col-sm-7">--}}
{{--                    <div class="section-title">--}}
{{--                        <span class="title-tag">Shop</span>--}}
{{--                        <h2>Top Trending</h2>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 col-md-4 col-sm-5 d-none d-sm-block">--}}
{{--                    <div class="shop-post-arrow arrow-style text-right">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row shop-post-slider mt-80">--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/Jade_Plant.jpg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">Sale</span>--}}
{{--                                <span class="price discounted">-15%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Dress</a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$390 <span>$480</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img7.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">New</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Bag</a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$290 <span>$300</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img4.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">New</span>--}}
{{--                                <span class="price discounted">-10%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Dress   </a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$450 <span>$510</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img5.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">Sale</span>--}}
{{--                                <span class="price discounted">-25%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Bag</a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$500 <span>$580</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img6.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">Sale</span>--}}
{{--                                <span class="price discounted">-15%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Bag</a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$390 <span>$480</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img7.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">New</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Bag </a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$290 <span>$300</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img9.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">New</span>--}}
{{--                                <span class="price discounted">-10%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Dress   </a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$450 <span>$510</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-12">--}}
{{--                    <div class="food-box shop-box">--}}
{{--                        <div class="thumb">--}}
{{--                            <img src="{{asset('assets/frontend/img/img10.jpeg')}}" alt="images" style="min-height: 355px !important;max-height: 355px !important;">--}}
{{--                            <div class="badges">--}}
{{--                                <span class="price">Sale</span>--}}
{{--                                <span class="price discounted">-25%</span>--}}
{{--                            </div>--}}
{{--                            <div class="button-group">--}}
{{--                                <a href="#"><i class="far fa-heart"></i></a>--}}
{{--                                <a href="#"><i class="far fa-sync-alt"></i></a>--}}
{{--                                <a href="#" data-toggle="modal" data-target="#"><i class="far fa-eye"></i></a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="desc">--}}
{{--                            <h4>--}}
{{--                                <a href="#">Dress</a>--}}
{{--                            </h4>--}}
{{--                            <span class="price">$500 <span>$580</span></span>--}}
{{--                            <a href="#" class="link"><i class="fal fa-arrow-right"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!--====== SHOP SECTION END ======-->
    <!--====== LATEST NEWS START ======-->
{{--    <section class="latest-news pt-115 pb-115">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6 col-md-8 col-sm-7">--}}
{{--                    <div class="section-title">--}}
{{--                        <span class="title-tag">Blog</span>--}}
{{--                        <h2>What our customers are saying</h2>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6 col-md-4 col-sm-5 d-none d-sm-block">--}}
{{--                    <div class="latest-post-arrow arrow-style text-right">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- Latest post loop -->--}}
{{--            <div class="row latest-post-slider mt-80">--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img1.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img2.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img3.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img4.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img5.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4">--}}
{{--                    <div class="latest-post-box">--}}
{{--                        <div class="post-img" style="background-image: url('{{asset('assets/frontend/img/img6.jpeg')}}');"></div>--}}
{{--                        <div class="post-desc">--}}
{{--                            <ul class="post-meta">--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-calendar-alt"></i>28th Aug 2020</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#"><i class="fal fa-user"></i>By Admin</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h4><a href="#">New Collection</a></h4>--}}
{{--                            <p>--}}
{{--                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor--}}
{{--                                incididunt ut labore et dolore.--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!--====== LATEST NEWS END ======-->
@endsection
