@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Thank You")
@section('styles')
    <style>

    </style>
@endsection
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{asset('assets/frontend/img/cart.jpg')}}');">
        <div class="container">
            <div class="breadcrumb-text">
                <span>styles that define you</span>
                <h2 class="page-title">Order Placed</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Order Placed</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <section class="cart-section pt-120 pb-120">

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12 text-center">
                    <h1 class="text-dark text-center">Thank You!</h1>
                    <h2 class="text-dark text-center">Your Order Has Been Placed Successfully</h2>
                    <h3 class="text-dark text-center">Your Order Will be Shipped Soon</h3>
                    <h4 class="text-dark text-center">For More Info Contact at: info@nurseryplant.com</h4>
                    <a href="{{url('/')}}" class="main-btn btn-filled">Continue Shoping</a>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
    </script>
@endsection
