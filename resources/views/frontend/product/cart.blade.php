@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('styles')
    <style>
        #shipping-form form label{
            color: #000000 !important;
        }
    </style>
@endsection
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{asset('assets/frontend/img/cart.jpg')}}');">
        <div class="container">
            <div class="breadcrumb-text">
                <span>styles that define you</span>
                <h2 class="page-title">Shopping Cart</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Cart</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <section class="cart-section pt-120 pb-120">

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12 col-sm-12 col-12">
                    <div class="w-100  mb-60">
                        <table class="table   mb-0">
{{--                            cw-cart-table--}}
                            <thead>
                            <tr>
                                <th></th>
                                <th  class="product-name">Product</th>
                                <th  class="product-qty">Quantity</th>
                                <th  class="product-price">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($cart->cartItems) > 0)
                            @foreach($cart->cartItems as $i=>$item)
                            <tr>
                                <td class="product-remove text-center cw-align">
                                    <a href="{{route('removeCart',$item->id)}}"><i class="fas fa-times" style="color: #000000"></i></a>
                                </td>
                                <td data-title="Product" class="has-title">
                                    <div class="product-thumbnail">
                                        <img src="{{asset('assets/frontend/img/product/'.$item->product->medias[0]->image)}}" alt="product_thumbnail">
                                    </div>
                                    <a href="#">{{$item->product->name}}</a>
                                </td>
                                <td class="quantity shop-detail-content cw-qty-sec cw-align has-title" data-title="Quantity">
                                    <b>{{$item->quantity}}</b>
{{--                                    <div class="quantity-box">--}}

{{--                                        <button type="button" data-id="{{++$i}}" class="minus-btn minus-btn">--}}
{{--                                            <i class="fal fa-minus"></i>--}}
{{--                                        </button>--}}
{{--                                        <input type="text" class="input-qty" name="name" value="{{$item->quantity}}">--}}
{{--                                        <button type="button" class="plus-btn plus-btn">--}}
{{--                                            <i class="fal fa-plus"></i>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
                                </td>
                                <td class="product-price  cw-align has-title" data-title="Price">
                                    <span class="product-currency"><b>$</b></span> <span class="product-amount"><b>{{$item->total}}</b></span>
                                </td>
                            </tr>
                            @endforeach
                            @else
                                <b>Cart is Empty</b>
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="3" class="">
                                    <a href="{{url('/')}}" class="main-btn btn-filled float-left">Continue Shoping</a>
{{--                                    <button class="main-btn btn-filled float-right">Update Cart</button>--}}
                                </td>
                                <td><b>Total: ${{$total}}</b></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row">
                        <div class=" col-lg-12 col-md-12">
                            <div class="cw-product-promo">
                                <table class="table cw-table-borderless">
                                    <tbody>
                                    <tr>
                                        <h2>Shipping Details</h2>
                                        <div class="row">
                                            <form id="shipping-form" method="post" action="{{route('saveShippingDetails')}}">
                                                @csrf
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">First Name
                                                </label>
                                                <input type="text" placeholder="First Name" name="first_name" required="">
                                                @if($errors->has('first_name'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('first_name')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">Last Name
                                                </label>
                                                <input type="text" placeholder="Last Name" name="last_name" required="">
                                                @if($errors->has('last_name'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('last_name')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">Phone</label>
                                                <input type="text" placeholder="Phone Number" name="phone" required="">
                                                @if($errors->has('phone'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('phone')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                    <label style="color: #000000 !important;">Country</label>
                                                    <input type="text" placeholder="Country" name="country" required="">
                                                @if($errors->has('country'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('country')}}</span>
                                                @endif
                                                </div>
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">City</label>
                                                <input type="text" placeholder="City" name="city" required="">
                                                @if($errors->has('city'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('city')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-xl-4 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">Postal Code</label>
                                                <input type="text" placeholder="Postal Code" name="postal_code" required="">
                                                @if($errors->has('postal_code'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('postal_code')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-12 input-group input-group-two mb-20">
                                                <label style="color: #000000 !important;">Address
                                                </label>
                                                <textarea name="address" style="min-height: 100px"></textarea>
                                                @if($errors->has('address'))
                                                    <span style="font-size: 12px; color: red">{{$errors->first('address')}}</span>
                                                @endif
                                            </div>
                                                <div class="col-12 input-group input-group-two mb-20 justify-content-around">
                                                <button type="submit"  class="main-btn btn-filled ">Proceed to Checkout</button>
                                                </div>
                                            </form>
                                        </div>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
    </script>
@endsection
