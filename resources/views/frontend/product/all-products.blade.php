@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('main-content')
    <!--====== BREADCRUMB PART START ======-->
    <section class="breadcrumb-area" style="background-image: url('{{$site_settings != null && $site_settings->listing_bg != null ? asset('assets/frontend/img/banner/'.$site_settings->listing_bg) : asset('assets/frontend/img/banner/Green.jpg')}}')">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">Products</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Products</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== BREADCRUMB PART END ======-->
    <!--====== SHOP SECTION START ======-->
    <section class="Shop-section pt-120 pb-120">
        <div class="container">
            <div class="row ">
                <!-- Shop Sidebar -->
                <div class="col-lg-8 col-md-10">
                    <div class="shop-products-wrapper">
                        <div class="shop-product-top">
                            <p>Showing {{$products->perPage()}} Of {{$products->total()}} results</p>
                        </div>
                        <div class="product-wrapper restaurant-tab-area">
                            <div class="row">
                                @if(count($products) > 0)
                                    @foreach($products as $product)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                    <div class="food-box shop-box">
                                        <div class="thumb product-listing">
                                            <img src="{{asset('assets/frontend/img/product/'.$product->medias[0]->image)}}" alt="images">
{{--                                            <div class="badges">--}}
{{--                                                <span class="price">Sale</span>--}}
{{--                                                <span class="price discounted">-15%</span>--}}
{{--                                            </div>--}}
                                            <div class="button-group">
                                                <a href="#"><i class="far fa-heart"></i></a>
{{--                                                <a href="#" data-toggle="modal" data-target="#quickViewModal"><i class="far fa-eye"></i></a>--}}
                                            </div>
                                        </div>
                                        <div class="desc">
                                            <h4>
                                                <a href="{{route('singleProduct',$product->slug)}}">{{$product->name}}</a>
                                            </h4>
                                            <span class="price">${{$product->price}}</span>
                                            <a href="{{route('singleProduct',$product->slug)}}" class="link"><i class="fal fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="pagination-wrap">
                        <ul class="d-flex justify-content-center">
                            {{ $products->links('vendor.pagination.default') }}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== SHOP SECTION END ======-->
@endsection
