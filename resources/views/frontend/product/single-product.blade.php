@extends('frontend.baseLayout')
@section('title',"Nursery Plants| Products")
@section('styles')
    <style>

    </style>
@endsection
@section('main-content')
    <section class="breadcrumb-area" style="background-image: url('{{$site_settings != null && $site_settings->product_bg != null ? asset('assets/frontend/img/banner/'.$site_settings->product_bg) : asset('assets/frontend/img/Jade_Plant.jpg')}}')">
        <div class="container">
            <div class="breadcrumb-text">
                <h2 class="page-title">{{$product->name}}</h2>
                <ul class="breadcrumb-nav">
                    <li><a href="#">Home</a></li>
                    <li class="active">Product</li>
                </ul>
            </div>
        </div>
    </section>
    <!--====== SHOP SECTION START ======-->
    <section class="Shop-section pt-120 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="shop-detail-image">
                        <div class="slider-for">
                            @foreach($product->medias as $image)
                            <div class="item">
                                <img src="{{asset('assets/frontend/img/product/'.$image->image)}}" alt="image" class="img-fluid"/>
                            </div>
                            @endforeach
                        </div>

                        <div class="slider-nav" style="min-width: 90% !important;max-width: 90% !important; padding-right: 5% !important;padding-left: 5% !important;">
                            @foreach($product->medias as $image)
                            <div class="item">
                                <img src="{{asset('assets/frontend/img/product/'.$image->image)}}" style="min-height: 100px !important; max-height: 100px !important; padding: 2px" alt="image"   draggable="false"/>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="shop-detail-content">
                        <h3 class="product-title mb-20" style="color: #000000 !important;">{{$product->name}}</h3>
{{--                        <span class="rating mb-20">--}}
{{--              <span class="text-yellow"><i class="far fa-star"></i></span>--}}
{{--              <span class="text-yellow"><i class="far fa-star"></i></span>--}}
{{--              <span class="text-yellow"><i class="far fa-star"></i></span>--}}
{{--              <span class="text-dark-white"><i class="far fa-star"></i></span>--}}
{{--              <span class="text-dark-white"><i class="far fa-star"></i></span>--}}
{{--              <span class="pro-review"> <span>1 Reviews</span>--}}
{{--              </span>--}}
{{--            </span>--}}
                        <div class="desc mb-20 pb-20 border-bottom">
                            <span class="price ">Price: $<b class="p-price">{{$product->price}}</b></span>
                        </div>
                        <div class="desc mb-20 pb-20 border-bottom">
                            @if(count($product->colors) > 0)
                            <span class="price ">Color:
                                @foreach($product->colors as $color)
                                <span style="width: 20px; height: 20px; background-color: {{$color->color}}"></span>
                                @endforeach
                            </span>

                            @else
                                <span class="price ">Color: None</span>
                            @endif
                        </div>

                        <div class="mt-20 mb-20">
                            <div class="d-inline-block other-info">
                                <h6 style="color: #111111 !important;">Availability:
                                    <span class="text-success ml-2">In Stock</span>
                                </h6>
                            </div>
                        </div>
                        <div class="short-descr mb-20">
                            <p>{{$product->description}}</p>
                        </div>
                        <div class="quantity-cart d-block d-sm-flex">
                            <div class="quantity-box">
                                <button type="button" class="minus-btn">
                                    <i class="fal fa-minus"></i>
                                </button>
                                <input type="text" min="1" class="input-qty" name="name" value="1">
                                <button type="button" class="plus-btn">
                                    <i class="fal fa-plus"></i>
                                </button>
                            </div>
                            <div class="cart-btn pl-40">
                                <button type="button" class="main-btn btn-border" id="btnAddToCart">Add to Cart</button>
                            </div>
                        </div>
                        <div class="other-info flex mt-20">
                            <h6 class="text-dark">Tags:</h6>
                            <ul>
                                @if(count($product->tags) > 0)
                                    @foreach($product->tags as $tag)
                                    <li class="list-inline-item mr-2">
                                        <a href="#" class="grey">{{$tag->name}}</a>
                                    </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="product-description mt-100">
                        <div class="tabs">
                            <ul class="nav nav-tabs justify-content-center">
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reviews">Reviews ({{$product->reviews->count()}})</a>
                                </li>
                            </ul>
                            <div class="tab-content pb-0">
                                <div class="tab-pane fade" id="reviews">
                                    <div class="news-details-box">
                                        <div class="comment-template">
                                            <h3 class="box-title">{{$product->reviews->count()}} Reviews</h3>
                                            <ul class="comments-list mb-100">
                                                @if($product->reviews->count() > 0)
                                                    @foreach($product->reviews as $review)
                                                    <li>
                                                    <div class="comment-desc">
                                                        <div class="desc-top">
                                                            <h6>{{$review->user->name}}</h6>
                                                            <span class="date">{{$review->created_at->format('M YY dd')}}</span>
                                                        </div>
                                                        <p style="color: #ffffff !important;">
                                                            {{$review->review}}
                                                        </p>
                                                    </div>
                                                </li>
                                                    @endforeach
                                                @else
                                                    <li>No Review has been added yet!</li>
                                                @endif
                                            </ul>
                                            <h3 class="box-title">Post Comment</h3>
                                            <div class="comment-form">
                                                <form action="{{route('postAReview')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                                    <div class="input-group input-group-two textarea mb-20">
                                                        <textarea name="review" placeholder="Type your Review...."></textarea>
                                                        <div class="icon"><i class="fas fa-pen"></i></div>
                                                    </div>
                                                    <div class="input-group  mt-30">
                                                        <button type="submit" class="main-btn btn-filled"><i class="far fa-comments"></i> Post
                                                            Review</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="loginModal">Login</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span style="font-size: 15px;color: red;display: none" id="inv-ep">Invalid Email or Password! Try Again</span>
                        <form method="post" id="loginModalForm" action="{{route('loginModal')}}">
                            @csrf
                            <div class="input-group input-group-two mb-20" style="border: 1px solid #ffffff">
                                <input type="email" id="email" placeholder="Email Address" name="email">
                                @if($errors->has('email'))
                                    <span style="color: red; font-size: 12px">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                            <div class="input-group input-group-two mb-30"  style="border: 1px solid #ffffff">
                                <input type="password" id="password" placeholder="Password" name="password">
                                @if($errors->has('password'))
                                    <span style="color: red; font-size: 12px">{{$errors->first('password')}}</span>
                                @endif
                            </div>
                            <button id="btnLogin" type="button" class="main-btn btn-block btn-filled mt-20 login-btn">Signup</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--====== SHOP SECTION END ======-->
@endsection
@section('scripts')
<script type="text/javascript">
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
        autoplay: true
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        prevArrow: false,
        nextArrow: false

    });
    $('.plus-btn').on('click',function () {
        var price=  {{$product->price}};
        let qty  =  $('.input-qty').val();
        var curr_quantity = $('.input-qty').val();
        curr_quantity = parseInt(curr_quantity)+1;
        $('.p-price').text((curr_quantity*price)+'.0');
    });
    $('.minus-btn').on('click',function () {
        var price=  {{$product->price}};
        let qty  =  $('.input-qty').val();
        var curr_quantity = $('.input-qty').val();
        curr_quantity = parseInt(curr_quantity)-1;
        $('.p-price').text((curr_quantity*price)+'.0');
    });

    $('#btnAddToCart').on('click', function () {
        @if(!auth()->check())
        $('#loginModal').modal('show');
        $('#btnLogin').on('click',function () {
            let csrfToken = $('meta[name="csrf-token"]').attr('content');
            let loginFormUrl = $('#loginModalForm').attr('action');
            $.ajax({
                url : loginFormUrl,
                method: 'POST',
                data : {
                    _token : csrfToken,
                    email : $('#email').val(),
                    password : $('#password').val()
                },
                datatype : 'json',
                success: function (response) {
                    if(response.status == 'success'){
                        addToCart(response.cart_id);
                    }else{
                        $('#loginModal').modal('show');
                        $('#inv-ep').css('display','block');
                    }
                }
            });
        });
        @else
            @if(auth()->user()->role != 'admin')
             addToCart({{auth()->user()->cart->id}});
             @else
                alert('Please Signout as Admin and Login as Buyer to Add Product to Cart');
            @endif
        @endif
    });
    function addToCart(c_id) {
        let quantity  =  $('.input-qty').val();
        let p_id = {{$product->id}};
        let csrfToken = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url : '{{route('addToCart')}}',
            method: 'POST',
            data : {
                _token : csrfToken,
                quantity : quantity,
                product_id : p_id,
                cart_id : c_id,
            },
            datatype : 'json',
            success: function (response) {
                console.log(response);
                if(response.status == 'success'){
                    console.log('yes ho gya');
                    window.location.href = "{{ route('cart')}}";
                }else{
                    console.log('No Nie Hua');
                }
            }
        });
    }
</script>
@endsection
