@extends('backend.baseLayout')
@section('title',"Nursery Plant  | Site Settings")
@section('main-content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Home Page</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('adminDashboard')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">Site Setting
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
                {{--                    <div class="form-group breadcrum-right">--}}
                {{--                        <div class="dropdown">--}}
                {{--                            <a href="{{route('addProduct')}}" type="button" class="btn btn-outline-success waves-effect waves-light">--}}
                {{--                                + Add New--}}
                {{--                            </a>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
            <div class="content-body">
                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-6 col-lg-6">
                                    <h5>Home Page Image</h5>
                                    <div class="d-flex align-items-center justify-content-center">
                                        @if($site_settings != null)
                                            @if($site_settings->home_image != null)
                                            <img src="{{asset('assets/frontend/img/banner/'.$site_settings->home_image)}}" class="img-fluid" alt="product image">
                                            @else
                                            <img src="{{asset('assets/frontend/img/placeholder.jpg')}}" class="img-fluid" alt="product image">
                                            @endif
                                        @else
                                            <img src="{{asset('assets/frontend/img/placeholder.jpg')}}" class="img-fluid" alt="product image">
                                        @endif
                                    </div>
                                    <form class="py-1" method="post" action="{{route('updateHomePageImage')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="site_id" value="{{$site_settings ? $site_settings->id : null}}">
                                        <div class="form-row">
                                            <div class="form-group col-lg-9 col-md-9 col-sm-12 col-12">
                                                <input type="file" name="image" accept="image/*" class="form-control" required>
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-12 col-12">
                                                <button class="btn btn-block btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-md-6">
                                    <h5>Home Page Text</h5>
                                    <form method="post" action="{{route('updateHomepageText')}}">
                                        @csrf
                                        <input type="hidden" name="site_id" value="{{$site_settings ? $site_settings->id : null}}">
                                        <div class="form-group">
                                            <label>Tag Line</label>
                                            <input type="text" required name="tagline" value="{{$site_settings != null && $site_settings->home_text_tagline != null ? $site_settings->home_text_tagline : '' }}" class="form-control" placeholder="Please Enter Tag Line, e.g. Yes our clothes are..">
                                        </div>
                                        <div class="form-group">
                                            <label>Homepage Heading <sub>E.g. Spring into summer with fashion..</sub></label>
                                            <textarea required name="heading" class="form-control" style="color: #ffffff" placeholder="Please Enter Heading Text, e.g. Spring into summer with fashion..">
                                                {{$site_settings != null && $site_settings->home_text_heading != null ? $site_settings->home_text_heading : '' }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-primary">Update</button>
                                        </div>
                                    </form>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
