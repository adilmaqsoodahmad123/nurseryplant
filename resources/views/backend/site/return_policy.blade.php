@extends('backend.baseLayout')
@section('title',"Nursery Plant | Site Settings")
@section('main-content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Home Page</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('adminDashboard')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">Site Setting
                                    </li>
                                    <li class="breadcrumb-item active">Return Policy
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
                {{--                    <div class="form-group breadcrum-right">--}}
                {{--                        <div class="dropdown">--}}
                {{--                            <a href="{{route('addProduct')}}" type="button" class="btn btn-outline-success waves-effect waves-light">--}}
                {{--                                + Add New--}}
                {{--                            </a>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
            <div class="content-body">
                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-12 col-lg-12">
                                    <h5>Return Policy</h5>
                                    <hr>
                                    <form method="post" action="{{route('updateReturnPolicyInfo')}}">
                                        @csrf
                                        <input type="hidden" name="site_id" value="{{$site_settings ? $site_settings->id : null}}">
                                        <div class="form-group">
                                            <label>Return Policy</label>
                                            <textarea style="min-height: 200px" required class="form-control" name="return_policy" placeholder="Add Return Policy Details Here...">
                                                {{$site_settings != null && $site_settings->return_policy != null ? $site_settings->return_policy : ''}}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-primary">Update/Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
