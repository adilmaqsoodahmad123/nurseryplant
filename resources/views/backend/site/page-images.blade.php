@extends('backend.baseLayout')
@section('title',"Nursery Plant  | Site Settings")
@section('main-content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Pages Images</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('adminDashboard')}}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active">Pages Images
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">--}}
                {{--                    <div class="form-group breadcrum-right">--}}
                {{--                        <div class="dropdown">--}}
                {{--                            <a href="{{route('addProduct')}}" type="button" class="btn btn-outline-success waves-effect waves-light">--}}
                {{--                                + Add New--}}
                {{--                            </a>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
            <div class="content-body">
                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-6 col-lg-6 p-3" style="border: 1px solid #ffffff !important; border-radius: 10px">
                                    <h5>Product Details Page Header Image</h5>
                                    <div class="d-flex align-items-center justify-content-center">
                                        @if($site_settings != null)
                                            @if($site_settings->product_bg != null)
                                                <img src="{{asset('assets/frontend/img/banner/'.$site_settings->product_bg)}}" class="img-fluid" alt="product image">
                                            @else
                                                <img src="{{asset('assets/frontend/img/noimg.png')}}" class="img-fluid" alt="product image">
                                            @endif
                                        @else
                                            <img src="{{asset('assets/frontend/img/noimg.png')}}" class="img-fluid" alt="product image">
                                        @endif
                                    </div>
                                    <form class="py-1" method="post" action="{{route('updateProductDetailHeaderImage')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="site_id" value="{{$site_settings ? $site_settings->id : null}}">
                                        <div class="form-row">
                                            <div class="form-group col-lg-8 col-md-8 col-sm-12 col-12">
                                                <input type="file" name="product_header" accept="image/*" class="form-control" required>
                                            </div>
                                            <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                                                <button class="btn btn-block btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6 p-3" style="border: 1px solid #ffffff !important; border-radius: 10px">
                                    <h5>All Product Listing Page Header Image</h5>
                                    <div class="d-flex align-items-center justify-content-center">
                                        @if($site_settings != null)
                                            @if($site_settings->listing_bg != null)
                                                <img src="{{asset('assets/frontend/img/banner/'.$site_settings->listing_bg)}}" class="img-fluid" alt="product image">
                                            @else
                                                <img src="{{asset('assets/frontend/img/noimg.png')}}" class="img-fluid" alt="product image">
                                            @endif
                                        @else
                                            <img src="{{asset('assets/frontend/img/noimg.png')}}" class="img-fluid" alt="product image">
                                        @endif
                                    </div>
                                    <form class="py-1" method="post" action="{{route('updateProductListingHeaderImage')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="site_id" value="{{$site_settings ? $site_settings->id : null}}">
                                        <div class="form-row">
                                            <div class="form-group col-lg-8 col-md-8 col-sm-12 col-12">
                                                <input type="file" name="listing_header" accept="image/*" class="form-control" required>
                                            </div>
                                            <div class="form-group col-lg-4 col-md-4 col-sm-12 col-12">
                                                <button class="btn btn-block btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
