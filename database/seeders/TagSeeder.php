<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'name' => Str::slug('new variety','-')
        ]);
        Tag::create([
            'name' => Str::slug('summer plants','-')
        ]);
        Tag::create([
            'name' => Str::slug('winter plants','-')
        ]);
//        Tag::create([
//            'name' => Str::slug('stylish handbags','-')
//        ]);
//        Tag::create([
//            'name' => Str::slug('stylish clutch','-')
//        ]);
//        Tag::create([
//            'name' => Str::slug('stylish shoe','-')
//        ]);
//        Tag::create([
//            'name' => Str::slug('girls shoe','-')
//        ]);
    }
}
