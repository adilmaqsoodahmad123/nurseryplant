<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Category::create([
            'name' => 'Jade Plants'
        ]);
        Category::create([
            'name' => 'Flowers'
        ]);
        Category::create([
            'name' => 'Winter Plants'
        ]);
        Category::create([
            'name' => 'Summer Plants'
        ]);
    }
}
