<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Collection;
use App\Models\Product;
use App\Models\Review;
use App\Models\Site;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $products = Product::with(['category','medias'])->orderBy('created_at','desc')->limit(6)->get();
        $categories = Category::with('products')->get();
        $clothing = 'Clothing';
        $handbags = 'Handbags';
        $shoes = 'Shoes';
        $accessories = 'Accessories';
        $clothing_data = Product::whereHas('category',function ($c) use ($clothing){
             $c->where('name','=',$clothing);
        })->orderBy('created_at','desc')->take(5)->get();
        $handbags_data = Product::whereHas('category',function ($c) use ($handbags){
            $c->where('name','=',$handbags);
        })->orderBy('created_at','desc')->take(5)->get();
        $shoes_data = Product::whereHas('category',function ($c) use ($shoes){
            $c->where('name','=',$shoes);
        })->orderBy('created_at','desc')->take(5)->get();
        $accessories_data = Product::whereHas('category',function ($c) use ($accessories){
            $c->where('name','=',$accessories);
        })->orderBy('created_at','desc')->take(5)->get();
        $site_settings = Site::first();

        return view('frontend/index',compact('products','categories','clothing_data','handbags_data','shoes_data','accessories_data','site_settings'));
    }
    public function allProductByCategory($id){
        $categories = Category::all();
        $site_settings = Site::first();
        $products = Product::with(['medias','category','tags'])
            ->where('category_id','=',$id)
            ->orderBy('created_at','desc')
            ->paginate(6);
        return view('frontend.product.all-products',compact('products','categories','site_settings'));
    }
    public function clothingCollection(){
        $categories = Category::with('products')->get();
        $clothing = 'Clothing';
        $products = Product::whereHas('category',function ($c) use ($clothing){
            $c->where('name','=',$clothing);
        })->orderBy('created_at','desc')->paginate(6);
        $site_settings = Site::first();
        return view('frontend.product.all-products',compact('products','categories','site_settings'));

//        $clothing_collection = Collection::with(['category','products'])->whereHas('category',function ($c) use ($clothing){
//            $c->where('name','=',$clothing);
//        })->orderBy('created_at','desc')->get();
//        return view('frontend.product.product-collection',compact('categories','clothing_collection'));
    }
    public function handbagsCollection(){
        $categories = Category::with('products')->get();
        $handbags = 'Handbags';
        $products = Product::whereHas('category',function ($c) use ($handbags){
            $c->where('name','=',$handbags);
        })->orderBy('created_at','desc')->paginate(6);
        $site_settings = Site::first();
        return view('frontend.product.all-products',compact('products','categories','site_settings'));
//        $clothing_collection = Collection::with(['category','products'])->whereHas('category',function ($c) use ($handbags){
//            $c->where('name','=',$handbags);
//        })->orderBy('created_at','desc')->get();
        return view('frontend.product.product-collection',compact('categories','clothing_collection'));
    }
    public function shoesCollection(){
        $site_settings = Site::first();
        $categories = Category::with('products')->get();
        $shoes = 'Shoes';
        $products = Product::whereHas('category',function ($c) use ($shoes){
            $c->where('name','=',$shoes);
        })->orderBy('created_at','desc')->paginate(6);
        return view('frontend.product.all-products',compact('products','categories','site_settings'));
//        $clothing_collection = Collection::with(['category','products'])->whereHas('category',function ($c) use ($shoes){
//            $c->where('name','=',$shoes);
//        })->orderBy('created_at','desc')->get();
//        return view('frontend.product.product-collection',compact('categories','clothing_collection'));
    }
    public function accessoriesCollection(){
        $categories = Category::with('products')->get();
        $accessories = 'Accessories';
        $products = Product::whereHas('category',function ($c) use ($accessories){
            $c->where('name','=',$accessories);
        })->orderBy('created_at','desc')->paginate(6);
        $site_settings = Site::first();
        return view('frontend.product.all-products',compact('products','categories','site_settings'));
//        $clothing_collection = Collection::with(['category','products'])->whereHas('category',function ($c) use ($accessories){
//            $c->where('name','=',$accessories);
//        })->orderBy('created_at','desc')->get();
//        return view('frontend.product.product-collection',compact('categories','clothing_collection'));
    }
    public function thankYou(){
        $categories = Category::with('products')->get();
        return view('frontend.product.thank-you',compact('categories'));
    }
    public function shippingPolicy(){
        $categories = Category::with('products')->get();
        $site_settings = Site::first();
        return view('frontend.shipping',compact('site_settings','categories'));
    }
    public function returnPolicy(){
        $categories = Category::with('products')->get();
        $site_settings = Site::first();
        return view('frontend.return_policy',compact('site_settings','categories'));
    }
    public function postAReview(Request $request){
        $review = Review::create([
           'product_id' => $request->input('product_id'),
           'review' => $request->input('review'),
           'user_id' => auth()->id()
        ]);
        if ($review){
            return redirect()->back();
        }else{
            dd('something error');
        }
    }

}
