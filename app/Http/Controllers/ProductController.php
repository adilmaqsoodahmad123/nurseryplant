<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\Shipping;
use App\Models\Site;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function allProducts(){
        $categories = Category::all();
        $products = Product::with(['medias','category','tags'])
            ->orderBy('created_at','desc')
            ->paginate(6);
        $site_settings = Site::first();
        return view('frontend.product.all-products',compact('products','categories','site_settings'));
    }
    public function singleProduct($slug){
        $categories = Category::all();
        $product = Product::with(['medias','category','tags','colors'])
            ->where('slug','=',$slug)
            ->firstOrFail();
        $site_settings = Site::first();
        return view('frontend.product.single-product',compact('product','categories','site_settings'));
    }

    public function checkout(){
        $categories = Category::all();
        $shipping_detail = Shipping::with('user')->where('user_id','=',auth()->user()->id)->latest()->firstOrFail();
        return view('frontend.product.checkout',compact('categories','shipping_detail'));
    }
}
