<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use HasFactory;

    protected $fillable = [
      'home_image','home_text_tagline','home_text_heading','shipping_policy','return_policy'
    ];
}
